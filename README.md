# README for Open Adventure

This code is a forward-port of the Crowther/Woods Adventure 2.5 from
1995, last version in the main line of Colossal Cave Adventure
development written by the original authors.  The authors have given
permission and encouragement for this release.

The file [history.txt](https://bitbucket.org/janlindblom/open-adventure/src/HEAD/history.txt)
contains a more detailed history of this game and its ancestors.

This project is called "Open Adventure" because it's not at all clear
how to number Adventure past 2.5 without misleading or causing
collisions or both.  See the history file for discussion.  The
original 6-character name on the PDP-10 has been reverted to for the
executable in order to avoid a collision with the BSD games port of
the ancestral 1977 version.

You can run a regression test on the code with 'make check'.

## Installation

Add this line to your application's Gemfile:

Install it yourself as:

    $ gem install open-adventure

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/janlindblom/open-adventure. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [BSD License](http://opensource.org/licenses/BSD).

## Code of Conduct

Everyone interacting in the Open::Adventure project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/janlindblom/open-adventure/src/HEAD/CODE_OF_CONDUCT.md).
